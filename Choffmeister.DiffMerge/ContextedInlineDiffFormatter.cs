﻿/*
 * Copyright (C) 2008, Johannes E. Schindelin <johannes.schindelin@gmx.de>
 * Copyright (C) 2009, Gil Ran <gilrun@gmail.com>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or
 * without modification, are permitted provided that the following
 * conditions are met:
 *
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 *
 * - Neither the name of the Git Development Community nor the
 *   names of its contributors may be used to endorse or promote
 *   products derived from this software without specific prior
 *   written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

using System;
using System.Collections.Generic;
using GitSharp.Core.Diff;

namespace Choffmeister.DiffMerge
{
    public class ContextedInlineDiffFormatter
    {
        private int _context;

        /// <summary>
        /// Create a new formatter with a default level of context.
        /// </summary>
        public ContextedInlineDiffFormatter()
        {
            Context = 6;
        }

        /// <summary>
        /// Change the number of lines of context to display.
        ///	</summary>
        ///	<param name="lineCount">
        /// Number of lines of context to see before the first
        /// modification and After the last modification within a hunk of
        /// the modified file.
        /// </param>
        public int Context
        {
            get
            {
                return _context;
            }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentException("context must be >= 0");
                }

                _context = value;
            }
        }

        /// <summary>
        /// Formats a list of edits in unified diff format
        /// </summary>
        /// <param name="out">where the unified diff is written to</param>
        /// <param name="a">the text A which was compared</param>
        /// <param name="b">the text B which was compared</param>
        /// <param name="edits">some differences which have been calculated between A and B</param>
        public List<ContextedInlineDiffBlock> FormatEdits(RawText a, RawText b, List<Edit> edits)
        {
            List<ContextedInlineDiffBlock> result = new List<ContextedInlineDiffBlock>();

            for (int curIdx = 0; curIdx < edits.Count; /* */)
            {
                ContextedInlineDiffBlock currentBlock = new ContextedInlineDiffBlock();
                result.Add(currentBlock);

                Edit curEdit = edits[curIdx];
                int endIdx = FindCombinedEnd(edits, curIdx);
                Edit endEdit = edits[endIdx];

                int aCur = Math.Max(0, curEdit.BeginA - _context);
                int bCur = Math.Max(0, curEdit.BeginB - _context);
                int aEnd = Math.Min(a.Size, endEdit.EndA + _context);
                int bEnd = Math.Min(b.Size, endEdit.EndB + _context);

                while (aCur < aEnd || bCur < bEnd)
                {
                    if (aCur < curEdit.BeginA || endIdx + 1 < curIdx)
                    {
                        currentBlock.Lines.Add(new ContextedInlineDiffLine(EditType.Empty, a.GetLine(aCur), aCur));

                        aCur++;
                        bCur++;
                    }
                    else if (aCur < curEdit.EndA)
                    {
                        currentBlock.Lines.Add(new ContextedInlineDiffLine(EditType.Delete, a.GetLine(aCur), aCur++));
                    }
                    else if (bCur < curEdit.EndB)
                    {
                        currentBlock.Lines.Add(new ContextedInlineDiffLine(EditType.Insert, b.GetLine(bCur), bCur++));
                    }

                    if (End(curEdit, aCur, bCur) && ++curIdx < edits.Count)
                    {
                        curEdit = edits[curIdx];
                    }
                }
            }

            return result;
        }

        private int FindCombinedEnd(IList<Edit> edits, int i)
        {
            int end = i + 1;
            while (end < edits.Count && (CombineA(edits, end) || CombineB(edits, end)))
            {
                end++;
            }
            return end - 1;
        }

        private bool CombineA(IList<Edit> e, int i)
        {
            return e[i].BeginA - e[i - 1].EndA <= 2 * _context;
        }

        private bool CombineB(IList<Edit> e, int i)
        {
            return e[i].BeginB - e[i - 1].EndB <= 2 * _context;
        }

        private static bool End(Edit edit, int a, int b)
        {
            return edit.EndA <= a && edit.EndB <= b;
        }
    }
}
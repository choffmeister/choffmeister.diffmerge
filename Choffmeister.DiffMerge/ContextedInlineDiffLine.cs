﻿using GitSharp.Core.Diff;

namespace Choffmeister.DiffMerge
{
    [System.Diagnostics.DebuggerDisplay("{EditType} {LineNumber} {Content}")]
    public class ContextedInlineDiffLine
    {
        public EditType EditType { get; set; }

        public string Content { get; set; }

        public int LineNumber { get; set; }

        public ContextedInlineDiffLine(EditType editType, string content, int lineNumber)
        {
            this.EditType = editType;
            this.Content = content;
            this.LineNumber = lineNumber;
        }
    }
}
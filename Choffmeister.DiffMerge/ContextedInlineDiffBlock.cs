﻿using System.Collections.Generic;

namespace Choffmeister.DiffMerge
{
    public class ContextedInlineDiffBlock
    {
        private readonly List<ContextedInlineDiffLine> _lines;

        public List<ContextedInlineDiffLine> Lines
        {
            get { return _lines; }
        }

        public ContextedInlineDiffBlock()
        {
            _lines = new List<ContextedInlineDiffLine>();
        }
    }
}
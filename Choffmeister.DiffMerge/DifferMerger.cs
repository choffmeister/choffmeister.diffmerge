﻿using System.Collections.Generic;
using System.IO;
using GitSharp.Core.Diff;
using GitSharp.Core.Merge;

namespace Choffmeister.DiffMerge
{
    public static class DifferMerger
    {
        public static List<ContextedInlineDiffBlock> Diff(string a, string b, int context = 5)
        {
            RawText rawText1 = new RawText(a.GetBytes().NormalizeLineEndings());
            RawText rawText2 = new RawText(b.GetBytes().NormalizeLineEndings());
            MyersDiff diff = new MyersDiff(rawText1, rawText2);
            ContextedInlineDiffFormatter formatter = new ContextedInlineDiffFormatter();
            formatter.Context = context;

            return formatter.FormatEdits(rawText1, rawText2, diff.Edits);
        }

        public static MergeResult Merge(string @base, string a, string b)
        {
            return MergeAlgorithm.Merge(
                new RawText(@base.GetBytes().NormalizeLineEndings()),
                new RawText(a.GetBytes().NormalizeLineEndings()),
                new RawText(b.GetBytes().NormalizeLineEndings())
            );
        }

        public static string FormatMerge(MergeResult mergeResult)
        {
            return FormatMerge(mergeResult, "base", "first", "second");
        }

        public static string FormatMerge(MergeResult mergeResult, string nameBase, string nameA, string nameB)
        {
            MergeFormatter formatter = new MergeFormatter();

            byte[] buff = null;
            using (MemoryStream ms = new MemoryStream())
            {
                using (BinaryWriter bw = new BinaryWriter(ms))
                {
                    formatter.FormatMerge(bw, mergeResult, nameBase, nameA, nameB);
                    ms.Position = 0;
                    buff = new byte[(int)ms.Length];
                    ms.Read(buff, 0, (int)ms.Length);
                }
            }

            return buff.GetString();
        }
    }
}
﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Choffmeister.DiffMerge
{
    public static class StringByteArrayExtensions
    {
        private static Encoding _encoding = Encoding.UTF8;

        public static Encoding Encoding
        {
            get { return _encoding; }
            set { _encoding = value; }
        }

        public static byte[] GetBytes(this string s)
        {
            return s != null ? _encoding.GetBytes(s) : new byte[0];
        }

        public static string GetString(this byte[] bytes)
        {
            return bytes != null ? _encoding.GetString(bytes) : string.Empty;
        }

        public static string NormalizeLineEndings(this string s)
        {
            return s.Replace("\r\n", "\n").Replace("\r", "\n");
        }

        public static byte[] NormalizeLineEndings(this byte[] bytes)
        {
            List<byte> list = bytes.ToList();

            int i = 0;
            while (i < list.Count)
            {
                if (list.Count - i > 2 && list[i] == 13 && list[i + 1] == 10)
                {
                    list[i] = 10;
                    list.RemoveAt(i + 1);
                }
                else if (list[i] == 13)
                {
                    list[i] = 10;
                }

                i++;
            }

            return list.ToArray();
        }
    }
}
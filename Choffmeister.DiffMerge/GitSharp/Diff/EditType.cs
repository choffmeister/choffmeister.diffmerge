﻿using System;

namespace GitSharp.Core.Diff
{
    /// <summary>
    /// Type of edit
    /// </summary>
    [Serializable]
    public enum EditType
    {
        /// <summary>
        /// Sequence B has inserted the region.
        /// </summary>
        Insert,

        /// <summary>
        /// Sequence B has removed the region.
        /// </summary>
        Delete,

        /// <summary>
        /// Sequence B has replaced the region with different content.
        /// </summary>
        Replace,

        /// <summary>
        /// Sequence A and B have zero length, describing nothing.
        /// </summary>
        Empty
    }
}
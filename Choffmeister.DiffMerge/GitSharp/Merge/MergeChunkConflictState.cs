﻿namespace GitSharp.Core.Merge
{
    /// <summary>
    /// A state telling whether a MergeChunk belongs to a conflict or not. The
    /// first chunk of a conflict is reported with a special state to be able to
    /// distinguish the border between two consecutive conflicts
    /// </summary>
    public enum MergeChunkConflictState
    {
        /// <summary>
        /// This chunk does not belong to a conflict
        /// </summary>
        NoConflict = 0,

        /// <summary>
        /// This chunk does belong to a conflict and is the first one of the conflicting chunks
        /// </summary>
        FirstConflictingRange = 1,

        /// <summary>
        /// This chunk does belong to a conflict but is not the first one of the conflicting chunks. It's a subsequent one.
        /// </summary>
        NextConflictingRange = 2
    }
}
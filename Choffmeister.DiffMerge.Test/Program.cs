﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using GitSharp.Core.Diff;
using GitSharp.Core.Merge;

namespace Choffmeister.DiffMerge.Test
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var test = Encoding.UTF8.GetBytes("a\r\n\r\nb").NormalizeLineEndings();

            RawText a = new RawText(File.ReadAllBytes("MarkupTest.txt").NormalizeLineEndings());
            RawText b = new RawText(File.ReadAllBytes("MarkupTest2.txt").NormalizeLineEndings());
            RawText c = new RawText(File.ReadAllBytes("MarkupTest3.txt").NormalizeLineEndings());

            MyersDiff diff = new MyersDiff(a, b);
            var edits = diff.Edits;
            FormatDiff(a, b, edits);

            ContextedInlineDiffFormatter contextedInlineDiffFormatter = new ContextedInlineDiffFormatter();
            contextedInlineDiffFormatter.Context = 2;
            var blokcs = contextedInlineDiffFormatter.FormatEdits(a, b, edits);

            MergeResult mergeResult = MergeAlgorithm.Merge(a, b, c);
            FormatMerge("base", "left", "right", mergeResult);

            Console.ReadKey();
        }

        private static void FormatDiff(RawText a, RawText b, List<Edit> edits)
        {
            DiffFormatter formatter = new DiffFormatter();
            formatter.FormatEdits(Console.OpenStandardOutput(), a, b, edits);
        }

        private static void FormatMerge(string aName, string bName, string cName, MergeResult mergeResult)
        {
            using (BinaryWriter a = new BinaryWriter(Console.OpenStandardOutput()))
            {
                MergeFormatter formatter = new MergeFormatter();
                formatter.FormatMerge(a, mergeResult, aName, bName, cName);
            }
        }
    }
}